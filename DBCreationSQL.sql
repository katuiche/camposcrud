if DB_ID('CAMPOSCRUD') IS NULL 
BEGIN
    CREATE DATABASE CAMPOSCRUD
END

IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'stages'))
BEGIN
    CREATE TABLE Stages (
        id INTEGER PRIMARY KEY,
        description VARCHAR(50) 
    )
    INSERT INTO Stages(description, id) VALUES ('Oportunidade', 1), ('Contrato', 2), ('Proposta', 3), ('Pedido', 4) 
END

IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'ActiveStates'))
BEGIN
    CREATE TABLE ActiveStates (
        id INTEGER PRIMARY KEY,
        description VARCHAR(50)
    )
    INSERT INTO ActiveStates (description, id) VALUES ('Ativo', 1), ('Inativo', 2)
END

IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'Clients'))
BEGIN
    CREATE TABLE Clients (
        id INTEGER IDENTITY PRIMARY KEY,
        name VARCHAR(100),
        inscritionNumber VARCHAR(11),
        address VARCHAR(200),
        stateId INTEGER REFERENCES ActiveStates(ID)
    )
END

IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'Products'))
BEGIN
    CREATE TABLE Products (
        id INTEGER IDENTITY PRIMARY KEY,
        description VARCHAR(50),
        unitaryPrice DECIMAL,
        stateId INTEGER REFERENCES ActiveStates(id)
    )
END

IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'Business'))
BEGIN
    CREATE TABLE Business (
        id INTEGER IDENTITY PRIMARY KEY,
        clientId INTEGER REFERENCES Clients(Id),
        stageId INTEGER REFERENCES Stages(Id),
        interestProductId INTEGER REFERENCES Products(Id),
        productQuantity INTEGER,
        businessValue DECIMAL
    )
END
