﻿using ProjetoCamposCRUD.Models.Database;
using ProjetoCamposCRUD.Models.Database.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjetoCamposCRUD.Models
{
    public class StageModel
    {
        public Stage GetStage(int stateId)
        {
            var result_state = DatabaseConnection.Current.Stages.Where(activeState => activeState.Id == stateId).FirstOrDefault();
            if (result_state == null)
                throw new ResultNotFoundException("The id " + stateId.ToString() + " was not located in stages table");
            return result_state;
        }
    }
}