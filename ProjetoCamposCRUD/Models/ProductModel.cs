﻿using ProjetoCamposCRUD.Models.Database;
using ProjetoCamposCRUD.Models.Database.Tables;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace ProjetoCamposCRUD.Models
{
    public class ProductModel
    {
        public void Insert(Product product)
        {
            DatabaseConnection.Current.Products.Add(product);
            DatabaseConnection.Current.SaveChanges();
        }

        public void Delete(int id)
        {
            var product = new Product { Id = id };
            DatabaseConnection.Current.Products.Remove(product);
            DatabaseConnection.Current.SaveChanges();
        }

        public void Update(Product product)
        {
            DatabaseConnection.Current.Products.Update(product);
            DatabaseConnection.Current.SaveChanges();
        }

        public List<Product> ListProducts()
        {
            return DatabaseConnection.Current.Products.Include(s => s.State).ToList();
        }

        public Product Find(int id)
        {
            var product = DatabaseConnection.Current.Products.Where(activeState => activeState.Id == id).FirstOrDefault();
            if (product == null)
                throw new ResultNotFoundException("The id " + id.ToString() + " was not located in the products table");
            return product;
        }


        public Product BuildProductInstance(string description, decimal unitaryPrice, int state)
        {
            var product = new Product();
            product.Description = description;
            product.UnitaryPrice = unitaryPrice;
            product.State = getState(state);
            return product;
        }

        public Product BuildProductInstance(int id, string description, decimal unitaryPrice, int state)
        {
            var product = new Product();
            product.Id = id;
            product.Description = description;
            product.UnitaryPrice = unitaryPrice;
            product.State = getState(state);
            return product;
        }

        ActiveState getState(int state_id)
        {      
            var activeStateModel = new ActiveStateModel();
            return activeStateModel.GetActiveState(state_id);
        }
    }
}