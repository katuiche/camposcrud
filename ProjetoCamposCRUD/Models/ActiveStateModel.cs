﻿using ProjetoCamposCRUD.Models.Database;
using ProjetoCamposCRUD.Models.Database.Tables;
using System.Collections.Generic;
using System.Linq;

namespace ProjetoCamposCRUD.Models
{
    public class ActiveStateModel
    {
        public ActiveState GetActiveState(int stateId)
        {
            var result_state = DatabaseConnection.Current.ActiveStates.Where(activeState => activeState.Id == stateId).FirstOrDefault();
            if (result_state == null)
                throw new ResultNotFoundException("The id " + stateId.ToString() + " was not located in active states table");
            return result_state;
        }

        public List<ActiveState> GetAllActiveStates()
        {
            return DatabaseConnection.Current.ActiveStates.ToList();
        }
    }
}