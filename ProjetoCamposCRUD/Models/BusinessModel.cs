﻿using ProjetoCamposCRUD.Models.Database;
using ProjetoCamposCRUD.Models.Database.Tables;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace ProjetoCamposCRUD.Models
{
    public class BusinessModel
    {
        public void Insert(Business business)
        {
            DatabaseConnection.Current.Businesses.Add(business);
            DatabaseConnection.Current.SaveChanges();
        }

        public void Delete(int id)
        {
            var business = new Business { Id = id };
            DatabaseConnection.Current.Businesses.Remove(business);
            DatabaseConnection.Current.SaveChanges();
        }

        public void Update(Business business)
        {
            DatabaseConnection.Current.Businesses.Update(business);
            DatabaseConnection.Current.SaveChanges();
        }

        public Business Find(int id)
        {
            var business = DatabaseConnection.Current.Businesses.Where(activeState => activeState.Id == id).FirstOrDefault();
            if (business == null)
                throw new ResultNotFoundException("The id " + id.ToString() + " was not located in the businnes table");
            return business;
        }

        public Business BuildBusinessInstance(int stage_id, int product_id, int client_id, int product_quantity)
        {
            var business = new Business();
            business.Stage = get_stage(stage_id);
            business.InterestProduct = get_product(product_id);
            business.Client = get_client(client_id);
            business.ProductQuantity = product_quantity;
            business.BusinessValue = business.ProductQuantity * business.InterestProduct.UnitaryPrice;
            return business;
        }

        public Business BuildBusinessInstance(int id, int stage_id, int product_id, int client_id, int product_quantity)
        {
            var business = new Business();
            business.Id = id;
            business.Stage = get_stage(stage_id);
            business.InterestProduct = get_product(product_id);
            business.Client = get_client(client_id);
            business.ProductQuantity = product_quantity;
            business.BusinessValue = business.ProductQuantity * business.InterestProduct.UnitaryPrice;
            return business;
        }

        Stage get_stage(int stage_id)
        {
            var stageModel = new StageModel();
            return stageModel.GetStage(stage_id);
        }

        Product get_product(int product_id)
        {
            var productModel = new ProductModel();
            return productModel.Find(product_id);
        }

        Client get_client(int client_id)
        {
            var clientModel = new ClientModel();
            return clientModel.Find(client_id);
        }

        public List<Business> ListProducts()
        {
            return DatabaseConnection.Current.Businesses.Include("State").ToList();
        }
    }
}