﻿using ProjetoCamposCRUD.Models.Database;
using ProjetoCamposCRUD.Models.Database.Tables;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace ProjetoCamposCRUD.Models
{
    public class ClientModel
    {
        public void Insert(Client client)
        {
            DatabaseConnection.Current.Clients.Add(client);
            DatabaseConnection.Current.SaveChanges();
        }

        public void Delete(int id)
        {
            var client = new Client { Id = id };
            DatabaseConnection.Current.Clients.Remove(client);
            DatabaseConnection.Current.SaveChanges();
        }

        public void Update(Client client)
        {
            DatabaseConnection.Current.Clients.Update(client);
            DatabaseConnection.Current.SaveChanges();
        }

        public List<Client> ListClients()
        {
            return DatabaseConnection.Current.Clients.Include("State").ToList();
        }

        public Client Find(int id)
        {
            var client = DatabaseConnection.Current.Clients.Where(activeState => activeState.Id == id).FirstOrDefault();
            if (client == null)
                throw new ResultNotFoundException("The id " + id.ToString() + " was not located in the products table");
            return client;
        }

        public Client BuildClientInstance(string name, string inscritionNumber, string adress, int state)
        {
            var client = new Client();
            client.Name = name;
            client.InscritionNumber = inscritionNumber;
            client.Adress = adress;
            client.State = getState(state);
            return client;
        }

        public Client BuildClientInstance(int id, string name, string inscritionNumber, string adress, int state)
        {
            var client = new Client();
            client.Id = id;
            client.Name = name;
            client.InscritionNumber = inscritionNumber;
            client.Adress = adress;
            client.State = getState(state);
            return client;
        }

        ActiveState getState(int state_id)
        {
            var activeStateModel = new ActiveStateModel();
            return activeStateModel.GetActiveState(state_id);
        }
    }
}