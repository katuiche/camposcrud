﻿using Microsoft.EntityFrameworkCore;
using ProjetoCamposCRUD.Models.Database.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjetoCamposCRUD.Models.Database
{
    public class DatabaseConnection: DbContext
    {
        public DbSet<Client> Clients { set; get; }
        public DbSet<Product> Products { set; get; }
        public DbSet<Business> Businesses { set; get; }
        public DbSet<ActiveState> ActiveStates { set; get; }
        public DbSet<Stage> Stages { set; get; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("server=localhost;database=CamposCRUD;trusted_connection=true;");
        }

        public static DatabaseConnection Current
        {
            get { return HttpContext.Current.Items["_EntityContext"] as DatabaseConnection; }
        }

    }
}