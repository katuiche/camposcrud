﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjetoCamposCRUD.Models.Database
{
    public class ResultNotFoundException : Exception
    {
        public ResultNotFoundException(string message)
            : base(message)
        {
        }
    }
}