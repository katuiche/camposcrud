﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjetoCamposCRUD.Models.Database.Tables
{
    public class Stage
    {
        public int Id { set; get; }
        public string Description { set; get; }
    }
}