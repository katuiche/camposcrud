﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace ProjetoCamposCRUD.Models.Database.Tables
{
    public class Product
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { set; get; }
        public string Description { set; get; }
        public decimal UnitaryPrice { set; get; }
        public ActiveState State { set; get; }
    }
}
