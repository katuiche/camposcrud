﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace ProjetoCamposCRUD.Models.Database.Tables
{
    public class ActiveState
    {
        [Key]
        public int Id { set; get; }
        public string Description { set; get; }
    }
}
