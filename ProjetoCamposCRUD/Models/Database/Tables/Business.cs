﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace ProjetoCamposCRUD.Models.Database.Tables
{
    public class Business
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { set; get; }
        public Client Client { set; get; }
        public Stage Stage { set; get; }
        public Product InterestProduct { set; get; }
        public int InterestProductId { set; get; }
        public int ProductQuantity { set; get; }
        public decimal BusinessValue { set; get; }
    }
}
