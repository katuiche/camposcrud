﻿using Newtonsoft.Json;
using ProjetoCamposCRUD.Models;
using ProjetoCamposCRUD.Models.Database.Tables;
using System.Collections.Generic;
using System.Web.Mvc;

namespace ProjetoCamposCRUD.Controllers
{
    class EditPageDataProduct
    {
        public List<ActiveState> States { get; set; }
        public Product Product { set; get; }
    }
    public class ProductController : Controller
    {
        public ActionResult Add(string description, decimal unitaryPrice, int state)
        {
            var productModel = new ProductModel();
            var product = productModel.BuildProductInstance(description, unitaryPrice, state);
            productModel.Insert(product);
            return RedirectToAction("ProductController", "Index");
        }

        public ActionResult Update(int id, string description, decimal unitaryPrice, int state)
        {
            var productModel = new ProductModel();
            var product = productModel.BuildProductInstance(id, description, unitaryPrice, state);
            productModel.Update(product);
            return RedirectToAction("ProductController", "Index");
        }


        public ActionResult Edit(int id)
        {
            var editPageData = new EditPageDataProduct();
            var activeStateModel = new ActiveStateModel();
            editPageData.States = activeStateModel.GetAllActiveStates();
            if (id != 0)
            {
                var productModel = new ProductModel();
                var product = productModel.Find(id);
                editPageData.Product = product;
            }
            ViewBag.ServerData = JsonConvert.SerializeObject(editPageData);
            ViewBag.PagePath = "~/Scripts/Pages/Products/Edit.jsx";
            return View("~/Views/Home/Index.cshtml");
        }
        public ActionResult Index()
        {
            var productModel = new ProductModel();
            var productList = productModel.ListProducts();
            ViewBag.ServerData = JsonConvert.SerializeObject(productList);
            ViewBag.PagePath = "~/Scripts/Pages/Products/Products.jsx";
            return View("~/Views/Home/Index.cshtml");
        }

    }
}