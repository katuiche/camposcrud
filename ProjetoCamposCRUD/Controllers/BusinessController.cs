﻿using Newtonsoft.Json;
using ProjetoCamposCRUD.Models;
using ProjetoCamposCRUD.Models.Database.Tables;
using System.Collections.Generic;
using System.Web.Mvc;

namespace ProjetoCamposCRUD.Controllers
{
    class EditPageDataBusiness
    {
        public List<ActiveState> States { get; set; }
        public Business Business { set; get; }
    }
    public class BusinessController : Controller
    {
        public ActionResult Add(int stage_id, int product_id, int client_id, int product_quantity)
        {
            var businessModel = new BusinessModel();
            var product = businessModel.BuildBusinessInstance(stage_id, product_id, client_id, product_quantity);
            businessModel.Insert(product);
            return RedirectToAction("BusinessController", "Index");
        }

        public ActionResult Update(int id, int stage_id, int product_id, int client_id, int product_quantity)
        {
            var businessModel = new BusinessModel();
            var business = businessModel.BuildBusinessInstance(id, stage_id, product_id, client_id, product_quantity);
            businessModel.Update(business);
            return RedirectToAction("BusinessController", "Index");
        }


        public ActionResult Edit(int id)
        {
            var editPageData = new EditPageDataBusiness();
            var activeStateModel = new ActiveStateModel();
            editPageData.States = activeStateModel.GetAllActiveStates();
            if (id != 0)
            {
                var businessModel = new BusinessModel();
                var business = businessModel.Find(id);
                editPageData.Business = business;
            }
            ViewBag.ServerData = JsonConvert.SerializeObject(editPageData);
            ViewBag.PagePath = "~/Scripts/Pages/Business/Edit.jsx";
            return View("~/Views/Home/Index.cshtml");
        }
        public ActionResult Index()
        {
            var businessModel = new BusinessModel();
            var productList = businessModel.ListProducts();
            ViewBag.ServerData = JsonConvert.SerializeObject(productList);
            ViewBag.PagePath = "~/Scripts/Pages/Business/Business.jsx";
            return View("~/Views/Home/Index.cshtml");
        }
    }
}