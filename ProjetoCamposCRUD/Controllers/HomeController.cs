﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjetoCamposCRUD.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            ViewBag.PagePath = "~/Scripts/Pages/Index.jsx";
            return View("~/Views/Home/Index.cshtml");
        }
    }
}