﻿using Newtonsoft.Json;
using ProjetoCamposCRUD.Models;
using ProjetoCamposCRUD.Models.Database.Tables;
using System.Collections.Generic;
using System.Web.Mvc;

namespace ProjetoCamposCRUD.Controllers
{
    class EditPageDataClient
    {
        public List<ActiveState> States { get; set; }
        public Client Client { set; get; }
    }
    public class ClientController : Controller
    {
        public ActionResult Add(string name, string inscritionNumber, string adress, int state)
        {
            var clientModel = new ClientModel();
            var client = clientModel.BuildClientInstance(name, inscritionNumber, adress, state);
            clientModel.Insert(client);
            return RedirectToAction("ClientController", "Index");
        }

        public ActionResult Update(int id, string name, string inscritionNumber, string adress, int state)
        {
            var clientModel = new ClientModel();
            var client = clientModel.BuildClientInstance(id, name, inscritionNumber, adress, state);
            clientModel.Update(client);
            return RedirectToAction("ClientController", "Index");
        }


        public ActionResult Edit(int id)
        {
            var editPageData = new EditPageDataClient();
            var activeStateModel = new ActiveStateModel();
            editPageData.States = activeStateModel.GetAllActiveStates();
            if (id != 0)
            {
                var clientModel = new ClientModel();
                var client = clientModel.Find(id);
                editPageData.Client = client;
            }
            ViewBag.ServerData = JsonConvert.SerializeObject(editPageData);
            ViewBag.PagePath = "~/Scripts/Pages/Clients/Edit.jsx";
            return View("~/Views/Home/Index.cshtml");
        }
        public ActionResult Index()
        {
            var clientModel = new ClientModel();
            var clientList = clientModel.ListClients();
            ViewBag.ServerData = JsonConvert.SerializeObject(clientList);
            ViewBag.PagePath = "~/Scripts/Pages/Clients/Clients.jsx";
            return View("~/Views/Home/Index.cshtml");
        }

    }
}