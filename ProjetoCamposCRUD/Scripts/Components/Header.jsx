﻿var { Button, Box, Toolbar } = window.MaterialUI;

export class Header extends React.Component {
    render() {
        return (
            <Toolbar>
                <Box justifyContent="center" alignItems="center">
                    <Button color="primary" href="/business" >Negócios</Button>
                    <Button color="primary" href="/clients" >Clientes</Button>
                    <Button color="primary" href="/product" >Produtos</Button>
                 </Box>
            </Toolbar>
        );
    }
}