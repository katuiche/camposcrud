﻿var { Button, Box, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, Paper } = window.MaterialUI;
import { Header } from "/Scripts/Components/Header.jsx";

const rows = window.serverData;

class ClientList extends React.Component {
    render() {
        return (
            <div>
                <Header />
                <TableContainer component={Paper}>
                    <Table aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell>Descrição</TableCell>
                                <TableCell align="center">Nome</TableCell>
                                <TableCell align="center">CPF/CNPJ</TableCell>
                                <TableCell align="center">Endereço</TableCell>
                                <TableCell align="center">Status</TableCell>
                                <TableCell align="center">Excluir</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {rows.map((row) => (
                                <TableRow href="./client" key={row.name}>
                                    <TableCell component="th" scope="row">
                                        {row.Name}
                                    </TableCell>
                                    <TableCell align="center">{row.InscritionNumber}</TableCell>
                                    <TableCell align="center">{row.Adress}</TableCell>
                                    <TableCell align="center">{row.State.Description}</TableCell>
                                    <TableCell align="center"><Button href="/client/delete/{row.Id}" color="secondary">X</Button></TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                <Button href="/client/edit/0" color="primary">Novo Cliente</Button>
            </div>
        );
    }
}

ReactDOM.render(<ClientList />, document.getElementById('content'));