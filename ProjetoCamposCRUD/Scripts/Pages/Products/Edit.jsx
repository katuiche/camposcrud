﻿var { TextField, Button, Container, TextField, MenuItem, Select, Box } = window.MaterialUI;
import { Header } from "/Scripts/Components/Header.jsx";


class ProductEdit extends React.Component {
    render() {
        var target = '/product/Add';
        if (window.serverData.EditObject != null)
            target = "/product/Update/";
        return (
            <Box justifyContent="center" alignItems="center">
                <Header />
                <form action={target} method="get">
                    <TextField name="description" id="description" label="Descrição" /><br/>
                    <TextField name="unitaryPrice" id="price" step="any"  type="number" label="Preço" /><br/>
                    <Select
                        labelId="state"
                        id="state"
                        name="state"
                    >
                        {window.serverData.States.map((activeState) => (
                            <MenuItem value={activeState.Id}>{activeState.Description}</MenuItem>
                        ))}
                    </Select><br />
                    {window.serverData.EditObject == null
                        ? <Button type="submit" color="primary">Criar</Button>
                        : <Button color="primary">Salvar</Button>
                    }
                </form>
            </Box >
        );
    }
}

ReactDOM.render(<ProductEdit />, document.getElementById('content'));