﻿var { Button, Box, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, Paper } = window.MaterialUI;
import { Header } from "/Scripts/Components/Header.jsx";

const rows = window.serverData;

class ProductList extends React.Component {
    render() {
        return (
            <div>
                <Header />
                <TableContainer component={Paper}>
                    <Table aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell>Descrição</TableCell>
                                <TableCell align="center">Preço</TableCell>
                                <TableCell align="center">Estado</TableCell>
                                <TableCell align="center">Excluir</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {rows.map((row) => (
                                <TableRow href="./Edit" key={row.name}>
                                    <TableCell component="th" scope="row">
                                        {row.Description}
                                    </TableCell>
                                    <TableCell align="center">{row.UnitaryPrice}</TableCell>
                                    <TableCell align="center">{row.State.Description}</TableCell>
                                    <TableCell align="center"><Button href="/product/delete/{row.Id}" color="secondary">X</Button></TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                <Button href="/product/edit/0" color="primary">Novo Produto</Button>
                </div>
        );
    }
}

ReactDOM.render(<ProductList />, document.getElementById('content'));