﻿var { Button, Box, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, Paper } = window.MaterialUI;
import { Header } from "/Scripts/Components/Header.jsx";

const rows = window.serverData;

class BusinessesList extends React.Component {
    render() {
        return (
            <div>
                <Header />
                <TableContainer component={Paper}>
                    <Table aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell>Cliente</TableCell>
                                <TableCell align="center">Etapa</TableCell>
                                <TableCell align="center">Produto de Interesse</TableCell>
                                <TableCell align="center">Quantidade do produto</TableCell>
                                <TableCell align="center">Valor do negócio</TableCell>
                                <TableCell align="center">Excluir</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {rows.map((row) => (
                                <TableRow href="./Edit" key={row.name}>
                                    <TableCell component="th" scope="row">
                                        {row.Stage.description}
                                    </TableCell>
                                    <TableCell align="center">{row.Product.Description}</TableCell>
                                    <TableCell align="center">{row.ProductQuantity}</TableCell>
                                    <TableCell align="center">{row.BusinessValue}</TableCell>
                                    <TableCell align="center"><Button href="/product/delete/{row.Id}" color="secondary">X</Button></TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                <Button href="/product/edit/0" color="primary">Novo Produto</Button>
            </div>
        );
    }
}

ReactDOM.render(<BusinessesList />, document.getElementById('content'));